# Comment utiliser les fichiers

- Téléchargez le ou les fichiers qui vous intéressent
- Allez sur mymaps de Google : https://mymaps.google.com
- Connectez-vous à votre compte Gmail
- En haut à gauche, cliquez sur le bouton "+ CREER UNE CARTE"
- Dans la nouvelle fenêtre, vous avez un carré en haut à gauche avec "Calque sans titre" et juste en dessous en bleu, "Importer". Cliquez sur "Importer"
- Sélectionnez le fichier que vous aurez téléchargé dans la première étape
- Patientez quelques secondes, et tout s'affiche
- Vous pouvez ensuite modifier les couleurs des arrondissements, des lignes de métro
- En cochant/decochant des calques et/ou des filtres (arrondissements/lignes de métro), vous pourrez affiner votre recherche 

# Ce que contiennent les fichiers

- Fichier des lycées : 
    - Secteur public
    - Lycées d'enseignement général
    - Lycées d'enseignement général et technologique
    - Lycées expérimentaux
- Fichier des collèges :
    - Secteur public
    - Collèges

# La source des données

[Education Nationale](https://data.education.gouv.fr/explore/dataset/fr-en-adresse-et-geolocalisation-etablissements-premier-et-second-degre/export/?disjunctive.nature_uai&disjunctive.nature_uai_libe&disjunctive.code_departement&disjunctive.code_region&disjunctive.code_academie&disjunctive.secteur_prive_code_type_contrat&disjunctive.secteur_prive_libelle_type_contrat&disjunctive.code_ministere&disjunctive.libelle_ministere&location=2,18.51495,-3.64057&basemap=jawg.streets) (opendata)
